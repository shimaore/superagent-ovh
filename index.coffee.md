    crypto = require 'crypto'

    class OVHAPI
      constructor: (options) ->
        {@appKey,@appSecret,@consumerKey} = options

      sign: (method, url, body, timestamp) ->
        hash = crypto
          .createHash 'sha1'
          .update @appSecret
          .update '+'
          .update @consumerKey
          .update '+'
          .update method
          .update '+'
          .update url
          .update '+'
          .update body
          .update '+'
          .update timestamp
          .digest('hex')
        '$1$' + hash

      headers: (method,url,body) ->
        timestamp = (Date.now() // 1000).toString 10
        {
        'X-Ovh-Application': @appKey
        'X-Ovh-Timestamp': timestamp
        'X-Ovh-Consumer': @consumerKey
        'X-Ovh-Signature': @sign method, url, body, timestamp
        }

Il faut d'abord avoir le body de la requête pour calculer la signature.

    Sign = (api) -> (request) ->
      if request._formData?
        throw new Error "Please use .send() to sign OVH requests."

Replace `request._data` with the body as a Buffer, so that we can explicitely compute the signature.

      data = request._data
      body = switch
        when not data?
          Buffer.allocUnsafe 0
        when typeof data is 'object'
          JSON.stringify data
        when typeof data is 'string'
          data
        else
          debug "Unable to Buffer", data
          Buffer.allocUnsafe 0
      request._data = body

      request.set api.headers request.method, request.url, body
      return

    sign = (options) -> Sign new OVHAPI options

    module.exports = {OVHAPI,Sign,sign}
