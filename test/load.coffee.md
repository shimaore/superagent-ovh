    (require 'chai').should()

    describe 'The module', ->
      it 'should load', ->
        require '..'
      it 'should sign', ->
        {OVHAPI} = require '..'
        api = new OVHAPI
          appKey:'7kbG7Bk7S9Nt7ZSV'
          appSecret:'EXEgWIz07P0HYwtQDs7cNIqCiQaWSuHF',
          consumerKey: 'MtSwSrPpNjqfVSmJhLbPyr2i45lSwPU1'
        api.sign 'GET', 'https://eu.api.ovh.com/1.0/domains/', Buffer.alloc(0), '1366560945'
          .should.eql '$1$d3705e8afb27a0d2970a322b96550abfc67bb798'

      it 'should add headers', ->
        {OVHAPI} = require '..'
        api = new OVHAPI
          appKey: '1'
          appSecret: '2'
          consumerKey: '3'
        r = api.headers 'GET', 'https://example.com', Buffer.from('hello')
        r.should.have.property 'X-Ovh-Application', '1'
        r.should.have.property 'X-Ovh-Timestamp'
        r.should.have.property 'X-Ovh-Consumer', '3'
        r.should.have.property 'X-Ovh-Signature'
