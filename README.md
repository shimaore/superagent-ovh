superagent-ovh
--------------

This module provides the [signature](https://docs.ovh.com/gb/en/customer/first-steps-with-ovh-api/) required for ovh.com's public API, as a plugin for [superagent](https://github.com/visionmedia/superagent#readme).

```shell
npm install --save superagent superagent-prefix superagent-ovh
```

```javascript
const ovh = require('superagent-ovh')

const ovh_sign = ovh.sign({
  appKey: …,
  appSecret: …,
  consumerKey: …
})

const request = require('superagent').agent()
  .use(require('superagent-prefix')('https://eu.api.ovh.com/1.0'))
  .accept('json')

// Just make sure `.use(ovh_sign)` is last in the chain.

request
  .get('/me')
  .use(ovh_sign)
  .end( (err,{body}) => {
    // Do something
  })

request
  .put('/me')
  .send(data)
  .use(ovh_sign)
  .end( (err,{body}) => { … } )
```
